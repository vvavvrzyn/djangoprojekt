from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.contrib import admin
admin.autodiscover()

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
      url(r'^admin/', include(admin.site.urls)),
      url(r'^dodaj/','ProjektZTW.taksowkarz.views.index'),
      url(r'^rezygnuj/','ProjektZTW.taksowkarz.views.zrezygnuj'),
      url(r'^rejestracja/','ProjektZTW.taksowkarz.views.rejestruj'),
      url(r'^$','ProjektZTW.taksowkarz.views.my_view',),
      url(r'^test/','ProjektZTW.taksowkarz.views.dodajKl'),
      url(r'^index/','ProjektZTW.taksowkarz.views.dodajKl'),
      url(r'^zalogowany/','ProjektZTW.taksowkarz.views.zalogowany'),
      url(r'^kursy/','ProjektZTW.taksowkarz.views.kursy'),
      url(r'^my_admin/jsi18n', 'django.views.i18n.javascript_catalog'),
      url(r'^css/(?P<path>.*)$', 'django.views.static.serve',
          {'document_root': settings.MEDIA_ROOT}),

    # Examples:
    # url(r'^$', 'ProjektZTW.views.home', name='home'),
    # url(r'^ProjektZTW/', include('ProjektZTW.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
