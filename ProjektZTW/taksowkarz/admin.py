# -*- coding: utf-8 -*-
'''
Created on 07-03-2012

@author: Administrator
'''
from django.contrib import admin

from ProjektZTW.taksowkarz.models import *

admin.site.register(Kierowca)
admin.site.register(Klient)
admin.site.register(Kurs)
