# Create your views here.
from django.template import RequestContext
from django.template import response
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
#from django.newforms import forms #@UnresolvedImport
from django.forms.models import ModelForm #@UnresolvedImport
from django import forms
from django.conf import settings
from datetime import datetime
from django.core import validators
from django.contrib.auth.models import User
from ProjektZTW.taksowkarz.models import *
from django.core.context_processors import request
from django.forms.widgets import Widget
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login

from django.contrib import auth
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.contrib.admin import widgets  
import datetime
import time
from datetime import datetime
#KursForm=forms.models.form_for_model(Kurs)
class DodajKurs(ModelForm):
    klient=forms.Field(label='',widget=forms.HiddenInput)
    kierowca=forms.Field(label='',widget=forms.HiddenInput)
    cena=forms.FloatField(label='',widget=forms.HiddenInput)
    data=forms.DateField(widget=forms.DateInput(format='%Y-%m-%d',),error_messages={'invalid': (u'zla data')})
    czas=forms.TimeField(widget=forms.TimeInput(format='%H:%M'),error_messages={'invalid': (u'zla godzina')})
    class Meta:
        model = Kurs
        exclude = ('czyZre',)
        
class DodajKlienta(forms.ModelForm):
    class Meta:
        model=Klient
# Creating a form to add an article.
def dodajKl(request):
    form=DodajKlienta()
    if request.method=='POST':
        data=request.POST.copy()
        form=DodajKlienta(data)
        if form.is_valid():
            print 'klej'
    return render_to_response('test.html',{'form':form}, context_instance=RequestContext(request))

def index(request):
    kurs=Kurs.objects.all().order_by('-id')
    kier=Kierowca.objects.all().order_by('-id')
    klient=Klient.objects.all().order_by('-id')
    if request.method=='POST':
            user = request.user
            form = DodajKurs()
            data=request.POST.copy()
            for kl in klient:
                if kl.user==user:
                #data['klient']=Klient.objects.get(pk=user.pk-1)
                    data['klient']=kl
            data['cena']=float(data['odleglosc'])*0.30 
        #data['data']= datetime.now()
            print data['czas']
            pom=True
            pom2=True
            for ob in kier:
                for k in kurs:
                    if k.kierowca==ob:
                        #print k.data
                        #print data['data']
                        #temp=time.strptime(data['czas'], "%H:%M")
                        #print temp
                        #temp2= time.mktime(temp)
                        
                        #print temp2 
                        #print temp2-datetime.timedelta(hours=1)
                        if k.data==data['data']:
                #if ob.czyWolny&pom:
                            pom2=False
                            if data['czas']-datetime.timedelta(hours=1)>k.czas&pom:
                                pom2=True
                            elif data['czas']==k.czas:
                                pom2=False
                                
                        else:
                            #if data['czas']-k.czas>1&pom:
                                pom2=True
                                
                if pom&pom2:    
                    data['kierowca']=ob
                    #ob.czyWolny=False
                    pom=False
                    pom2=False
                    ob.save()
                    
        
            form = DodajKurs(data)
            print form.is_valid()
           # form.save()
            if form.is_valid():
                print "klej2"
                form.save()
    else:
        form=DodajKurs()
    return render_to_response('dodaj.html',{'form':form, 'kier':kier}, context_instance=RequestContext(request))

def zrezygnuj(request):
    klient=Klient.objects.all().order_by('-id')
    user = request.user
    for kl in klient:
                if kl.user==user:
                    kursy=Kurs.objects.filter(klient=kl)
    return render_to_response('zrezygnuj.html',
                              {'kursy': kursy},
                              context_instance=RequestContext(request))
class Rejestracje(UserCreationForm):
    imie = forms.CharField(max_length=100)
    nazwisko = forms.CharField(max_length=100)
    email = forms.EmailField()
    nrtel=forms.CharField(max_length=100)
    
def rejestruj(request):
    form=Rejestracje()
    us=User.objects.all().order_by('id')
    i=0
    if request.method == 'POST':
        data = request.POST.copy()
        #form = UserCreationForm(data)
        form = Rejestracje(data)
        if form.is_valid():
            form.save()
            for uz in us:
                print uz.pk
            print data['imie']
            print User.objects.filter(pk=uz.pk)
            dodaj=DodajKlienta()
            data2=request.POST.copy()
            data2['user']=User.objects.filter(pk=uz.pk)
            data2['imieK']=data['imie']
            data2['nazwiskoK']=data['nazwisko']
            data2['email']=data['email']
            data2['nrtelK']=data['nrtel']
            data2['loginK']=data['username']
            data2['hasloK']=data['password1']
            dodaj=DodajKlienta(data2)
            if dodaj.is_valid():
                form.save()
                dodaj=DodajKlienta(data2)
                dodaj.save()
                return HttpResponseRedirect('/')
   

    return render_to_response("register.html", {
        'form' : form},context_instance = RequestContext(request))
    
class Logowanie(forms.Form):
    login = forms.CharField(max_length=100)
    haslo = forms.CharField(widget=forms.PasswordInput(render_value=False)) 
    
def my_view(request):
    form = Logowanie()
    if request.method == "POST":
        username = request.POST['login']
        password = request.POST['haslo']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                kier=Kierowca.objects.all().order_by('id')
                klient=Klient.objects.all().order_by('-id')
                
                for kl in klient:
                    if kl.user==user:
                        return HttpResponseRedirect('/zalogowany/')
                    
                for k in kier:
                    if k.user==user:
                        return HttpResponseRedirect('/kursy/')
                #return HttpResponseRedirect('/dodaj/')
    
    
    return render_to_response("loguj.html", {
        'form' : form},context_instance = RequestContext(request))
        

def zalogowany(request):
    user=request.user
    return render_to_response("witaj.html",{'user':user},context_instance = RequestContext(request))


def kursy(request):
    kier=Kierowca.objects.all().order_by('id')
    user=request.user
    for kl in kier:
                if kl.user==user:
                    kursy=Kurs.objects.filter(kierowca=kl,czyZre=False)
    return render_to_response('kursy.html',
                              {'kursy': kursy},
                              context_instance=RequestContext(request))