# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

class Kierowca(models.Model):
        user = models.ForeignKey(User, unique=True)
        imie=models.CharField(max_length=40, verbose_name='Imię')
        nazwisko=models.CharField(max_length=50, verbose_name='Nazwisko')
        nrtel=models.CharField(max_length=9, verbose_name='Numer telefonu')
        login=models.CharField(max_length=15, verbose_name='Login')
        haslo=models.CharField(max_length=20, verbose_name='Hasło')
        czyWolny=models.BooleanField(verbose_name="Czy wolny")
        class Meta:
                verbose_name='Kierowca'
                verbose_name_plural='Kierowcy'
        def __str__(self):
                return self.nazwisko
        def __unicode__(self):
                return self.nazwisko
            
class Klient(models.Model):
        user = models.ForeignKey(User, unique=True)
        imieK=models.CharField(max_length=40, verbose_name='Imię')
        nazwiskoK=models.CharField(max_length=50, verbose_name='Nazwisko')
        email=models.EmailField(max_length=45, verbose_name='Email')
        nrtelK=models.CharField(max_length=9, verbose_name='Numer telefonu')
        loginK=models.CharField(max_length=15, verbose_name='Login')
        hasloK=models.CharField(max_length=20, verbose_name='Hasło')
        class Meta:
                verbose_name='Klient'
                verbose_name_plural='Klienci'
        def __str__(self):
                return self.nazwiskoK
        def __unicode__(self):
                return self.nazwiskoK

class Kurs(models.Model):
        kierowca=models.ForeignKey(Kierowca, blank=True, null=True, on_delete=models.SET_NULL, verbose_name='Kierowca')
        klient=models.ForeignKey(Klient, verbose_name='Klient')
        ulicaP=models.CharField(max_length=45, verbose_name='Ulica poczatkowa')
        nrlokP=models.CharField(max_length=45, verbose_name='Nr lokalu poczatkowego')
        ulicaK=models.CharField(max_length=45, verbose_name='Ulica końcowa')
        nrlokK=models.CharField(max_length=45, verbose_name='Nr lokalu koncowego')
        odleglosc=models.FloatField(verbose_name='Odległość')
        cena=models.FloatField(verbose_name='Cena')
        data=models.DateField(verbose_name="Data kusu")
        czas=models.TimeField(verbose_name="Godzina kursu")
        czyZre=models.BooleanField(verbose_name="Czy kurs zrealizowany")
        class Meta:
                verbose_name='Kurs'
                verbose_name_plural='Kursy'
        def __str__(self):
                return str(self.klient)
        def _unicode__(self):
                return str(self.kierowca)